package com.test.mobilephonebuyer

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.test.mobilephonebuyer.adapter.MobileListAdapter
import com.test.mobilephonebuyer.config.waitId
import com.test.mobilephonebuyer.ui.main.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {


    @get:Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setUp() {

    }

    @Test
    fun user_click_sort_list_should_see_dialog_sort() {
        activityRule.launchActivity(null)
        onView(withId(R.id.fr_sort)).perform(click())
        onView(withId(R.id.rdo_group)).check(matches(isDisplayed()))
    }


    @Test
    fun user_click_mobile_item_should_see_mobile_detail() {
        activityRule.launchActivity(null)
        onView(withId(R.id.rv_mobile)).check(matches(isDisplayed()))
        onView(isRoot()).perform(
            waitId(
                R.id.iv_favorite,
                TimeUnit.SECONDS.toMillis(15)
            )
        )

        onView(withId(R.id.rv_mobile))
            .perform(RecyclerViewActions.actionOnItemAtPosition<MobileListAdapter.ItemHolder>(0, click()))
        onView(withId(R.id.tv_title)).check(matches(isDisplayed()))
    }

}