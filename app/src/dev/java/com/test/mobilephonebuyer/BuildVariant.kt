package com.test.mobilephonebuyer

import com.test.mobilephonebuyer.enums.ApplicationMode

object BuildVariant {
    var applicationMode = ApplicationMode.Developer
}