package com.test.mobilephonebuyer

import com.test.mobilephonebuyer.api.MobileAPI
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.presenter.MobileListPresenter
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class MobileListPresenterTest {

    private lateinit var presenter: MobileListPresenter

    @Mock
    private lateinit var view: MobileListPresenter.View
    @Mock
    private lateinit var api: MobileAPI

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MobileListPresenter(api, RxScheduler.rxScheduler)
        presenter.injectView(view)
    }

    @Test
    @Throws(Exception::class)
    fun `get mobile list should have data`() {
        val collection = mutableListOf<GraphMobile>()
        Mockito.`when`(api.getMobileList()).thenReturn(Observable.just(collection))

        presenter.getMobileList()

        Mockito.verify(view).loading()
        Mockito.verify(view).showList(collection)
        Mockito.verify(view).dismissLoading()
    }

    @Test
    @Throws(Exception::class)
    fun `get mobile list error should have exception on stream`() {
        val exception = Throwable()
        Mockito.`when`(api.getMobileList()).thenReturn(Observable.error(exception))

        presenter.getMobileList()

        Mockito.verify(view).loading()
        Mockito.verify(view).showMessageError(exception.message)
        Mockito.verify(view).dismissLoading()
    }

    @Test
    @Throws(Exception::class)
    fun `get mobile image list with mobile id should have data`() {
        val collection = mutableListOf<GraphImage>()
        val mobileId = 1
        Mockito.`when`(api.getImagesWithMobileId(mobileId)).thenReturn(Observable.just(collection))

        presenter.getImagesWithMobileId(mobileId)

        Mockito.verify(view).loading()
        Mockito.verify(view).showImages(collection)
        Mockito.verify(view).dismissLoading()
    }

    @Test
    @Throws(Exception::class)
    fun `get mobile image list with mobile id error should have exception on stream`() {
        val exception = Throwable()
        val mobileId = 1
        Mockito.`when`(api.getImagesWithMobileId(mobileId)).thenReturn(Observable.error(exception))

        presenter.getImagesWithMobileId(mobileId)

        Mockito.verify(view).loading()
        Mockito.verify(view).showMessageError(exception.message)
        Mockito.verify(view).dismissLoading()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter.onDestroy()
    }
}