package com.test.mobilephonebuyer

import com.test.mobilephonebuyer.rx.RxThread
import io.reactivex.schedulers.Schedulers

object RxScheduler {
    val rxScheduler = RxThread(Schedulers.trampoline(), Schedulers.trampoline())
}