package com.test.mobilephonebuyer.ui.main

import com.test.mobilephonebuyer.model.GraphMobile

interface OnSentEventMainActivityListener {
    fun reLoadMobileList()
    fun onMobileDetail(mobile: GraphMobile)
    fun onDeleteFavorite(mobile: GraphMobile)
    fun onClickFavorite(mobile: GraphMobile)
}