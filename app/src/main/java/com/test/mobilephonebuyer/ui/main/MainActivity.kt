package com.test.mobilephonebuyer.ui.main

import android.os.Bundle
import android.widget.Toast
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.adapter.MainPagerAdapter
import com.test.mobilephonebuyer.custom.dialog.DialogSortList
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.presenter.MobileListPresenter
import com.test.mobilephonebuyer.ui.base.activity.BindingActivity
import com.test.mobilephonebuyer.ui.favorite.FavoriteListFragment
import com.test.mobilephonebuyer.ui.favorite.OnEventFavoriteListFragmentListener
import com.test.mobilephonebuyer.ui.list.MobileListFragment
import com.test.mobilephonebuyer.ui.list.OnEventMobileListFragmentListener
import javax.inject.Inject

class MainActivity : BindingActivity<com.test.mobilephonebuyer.databinding.ActivityMainBinding>(),
        MobileListPresenter.View, OnSentEventMainActivityListener {

    @Inject
    lateinit var presenter: MobileListPresenter

    @Inject
    lateinit var pagerAdapter: MainPagerAdapter
    private var currentSort = SortEnum.RATING
    private var currentMobile: GraphMobile? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initListener()
        presenter.injectView(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }


    private fun initView() {
        binding.viewpager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewpager)
        binding.viewpager.post {
            presenter.getMobileList()
        }
    }

    private fun initListener() {
        binding.frSort.setOnClickListener {
            val dialog = DialogSortList.newInstance(currentSort)
            dialog.setOnSortList {
                currentSort = it
                notifySortAllFragments(currentSort)
            }
            dialog.show(supportFragmentManager)
        }
    }


    override fun showList(list: MutableList<GraphMobile>) {
        val listenerMobileList = getListener<OnEventMobileListFragmentListener>(MainPagerAdapter.Page.LIST)
        listenerMobileList?.showMobileList(list)

        getListener<OnEventFavoriteListFragmentListener>(MainPagerAdapter.Page.FAVORITE)
                ?.reLoadView()
    }

    override fun showImages(images: MutableList<GraphImage>) {
        currentMobile?.let {
            val values = Pair(it, images)
            getListener<OnEventBaseListener>(getCurrentPage())
                    ?.showMobileDetail(values)
        }
    }

    override fun showMessageError(message: String?) {
        message?.let { Toast.makeText(this, it, Toast.LENGTH_SHORT).show() }
    }

    override fun loading() {
        getListener<OnEventBaseListener>(getCurrentPage())
                ?.onLoadingState(true)
    }

    override fun dismissLoading() {
        getListener<OnEventBaseListener>(getCurrentPage())
                ?.onLoadingState(false)
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }


    override fun reLoadMobileList() {
        presenter.getMobileList()
    }

    override fun onMobileDetail(mobile: GraphMobile) {
        this.currentMobile = mobile
        presenter.getImagesWithMobileId(mobile.id)
    }

    override fun onDeleteFavorite(mobile: GraphMobile) {
        val listenerMobileList = getListener<OnEventMobileListFragmentListener>(MainPagerAdapter.Page.LIST)
        listenerMobileList?.onDeleteFavorite(mobile)
    }

    override fun onClickFavorite(mobile: GraphMobile) {
        val listenerFavoriteListener = getListener<OnEventFavoriteListFragmentListener>(MainPagerAdapter.Page.FAVORITE)
        listenerFavoriteListener?.showFavoriteList(mobile)
    }

    private fun getCurrentPage(): MainPagerAdapter.Page {
        val current = binding.viewpager.currentItem
        return MainPagerAdapter.Page.fromPosition(current)
    }

    private fun notifySortAllFragments(sortEnum: SortEnum) {
        val enums = MainPagerAdapter.Page.values()
        enums.forEach { p ->
            val listener = getListener<OnEventBaseListener>(p)
            listener?.onSortList(sortEnum)
        }
    }

    private fun <L : OnEventBaseListener> getListener(page: MainPagerAdapter.Page): L? {
        var listener: OnEventBaseListener? = null
        val fragment = when (page) {
            MainPagerAdapter.Page.LIST -> supportFragmentManager.fragments.find { it is MobileListFragment }
            MainPagerAdapter.Page.FAVORITE -> supportFragmentManager.fragments.find { it is FavoriteListFragment }
            else -> null
        }
        listener = if (fragment != null) fragment as OnEventBaseListener else null
        return if (listener != null) listener as L else null
    }

    override fun injectDependencies(graph: ApplicationComponent) {
        graph.plus(MainModule(this))
                .injectTo(this)
    }
}
