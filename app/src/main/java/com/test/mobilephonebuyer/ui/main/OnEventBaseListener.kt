package com.test.mobilephonebuyer.ui.main

import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile

interface OnEventBaseListener {
    fun onLoadingState(isLoading: Boolean)
    fun showMobileDetail(values: Pair<GraphMobile, MutableList<GraphImage>>)
    fun onSortList(sortEnum: SortEnum)
}