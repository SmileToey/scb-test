package com.test.mobilephonebuyer.ui.main

import com.test.mobilephonebuyer.di.PerActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(
    modules = arrayOf(
        MainModule::class
    )
)
interface MainComponent {
    fun injectTo(activity: MainActivity)
}