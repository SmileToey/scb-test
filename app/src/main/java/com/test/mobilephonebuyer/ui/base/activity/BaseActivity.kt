package com.test.mobilephonebuyer.ui.base.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import com.test.mobilephonebuyer.AppStart
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.extensions.hideKeyboard


abstract class BaseActivity : AppCompatActivity() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        injectDependencies(AppStart.graph)
    }

    override fun onSaveInstanceState(oldInstanceState: Bundle) {
        super.onSaveInstanceState(oldInstanceState)
        oldInstanceState.clear()
    }




    abstract fun injectDependencies(graph: ApplicationComponent)

    override fun onBackPressed() {
        super.onBackPressed()
        hideKeyboard()
    }
}