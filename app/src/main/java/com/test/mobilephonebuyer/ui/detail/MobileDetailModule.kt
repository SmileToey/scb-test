package com.test.mobilephonebuyer.ui.detail

import androidx.appcompat.app.AppCompatActivity
import com.test.mobilephonebuyer.di.module.ActivityModule
import dagger.Module

@Module
class MobileDetailModule(activity: AppCompatActivity) : ActivityModule(activity) {


}