package com.test.mobilephonebuyer.ui.main

import androidx.appcompat.app.AppCompatActivity
import com.test.mobilephonebuyer.di.module.ActivityModule
import dagger.Module

@Module
class MainModule(activity: AppCompatActivity) : ActivityModule(activity) {


}