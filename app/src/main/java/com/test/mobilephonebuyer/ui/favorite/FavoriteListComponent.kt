package com.test.mobilephonebuyer.ui.favorite

import com.test.mobilephonebuyer.di.PerActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(
        modules = arrayOf(
                FavoriteListModule::class
        )
)
interface FavoriteListComponent {
    fun injectTo(fragment: FavoriteListFragment)
}