package com.test.mobilephonebuyer.ui.base.fragment

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.test.mobilephonebuyer.AppStart
import com.test.mobilephonebuyer.di.component.ApplicationComponent

abstract class BaseFragment : Fragment() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies(AppStart.graph)
    }

    abstract fun injectDependencies(graph: ApplicationComponent)
}