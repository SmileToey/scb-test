package com.test.mobilephonebuyer.ui.base.activity

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BindingActivity<B : ViewDataBinding> : BaseActivity() {

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        intent?.extras?.let {
            getExtra(it)
        }
    }

    abstract fun getLayoutId(): Int

    @CallSuper
    protected open fun getExtra(bundle: Bundle) {
        bundle.clear()
    }
}