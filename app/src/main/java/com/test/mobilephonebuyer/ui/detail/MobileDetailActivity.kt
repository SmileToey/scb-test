package com.test.mobilephonebuyer.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.adapter.PreviewImageAdapter
import com.test.mobilephonebuyer.databinding.ActivityMobileDetailBinding
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.extensions.getList
import com.test.mobilephonebuyer.extensions.toCurrency
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.ui.base.activity.BindingActivity
import javax.inject.Inject

class MobileDetailActivity : BindingActivity<ActivityMobileDetailBinding>() {

    @Inject
    lateinit var adapter: PreviewImageAdapter

    private var mobile: GraphMobile? = null
    private var images: MutableList<GraphImage> = mutableListOf()

    companion object {
        const val KEY_BUNDLE = "key_bundle"
        const val KEY_MOBILE = "key_mobile"
        const val KEY_IMAGES = "key_images"
        fun starter(context: Context, mobile: GraphMobile, images: MutableList<GraphImage>) {
            val intent = Intent(context, MobileDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(KEY_MOBILE, mobile)
            bundle.putParcelableArray(KEY_IMAGES, images.toTypedArray())
            intent.putExtra(KEY_BUNDLE, bundle)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initListener()
    }

    override fun getExtra(bundle: Bundle) {
        val values = bundle.getBundle(KEY_BUNDLE)
        values?.let {
            mobile = it.getParcelable(KEY_MOBILE)
            images = it.getList(KEY_IMAGES)
        }
        super.getExtra(bundle)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_mobile_detail
    }

    private fun initView() {
        mobile?.let {
            binding.tvTitle.text = it.brand
            binding.tvDes.text = it.description
            binding.tvPrice.text = String.format(getString(R.string.item_mobile_string_format_price), it.price.toCurrency())
            binding.tvRating.text = String.format(getString(R.string.item_mobile_string_format_rating), it.rating)
        }
        adapter.setImages(images)
        binding.rvPreviewImages.adapter =adapter
    }

    private fun initListener() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun injectDependencies(graph: ApplicationComponent) {
        graph.plus(MobileDetailModule(this))
                .injectTo(this)
    }
}
