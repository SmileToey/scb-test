package com.test.mobilephonebuyer.ui.list


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.adapter.MobileListAdapter
import com.test.mobilephonebuyer.databinding.FragmentMobileListBinding
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.ui.base.fragment.BindingFragment
import com.test.mobilephonebuyer.ui.detail.MobileDetailActivity
import com.test.mobilephonebuyer.ui.main.OnSentEventMainActivityListener
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MobileListFragment : BindingFragment<FragmentMobileListBinding>(), OnEventMobileListFragmentListener {

    @Inject
    lateinit var adapter: MobileListAdapter
    private var listener: OnSentEventMainActivityListener? = null


    companion object {
        fun newInstance(context: Context): Fragment {
            return instantiate(context, MobileListFragment::class.java.name)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as OnSentEventMainActivityListener
        if (listener == null) {
            throw ClassCastException("$context must implement OnSentEventMainActivityListener")
        }
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_mobile_list
    }


    private fun initView() {
        adapter.setCurrentMode(MobileListAdapter.Mode.LIST)
        binding.rvMobile.adapter = adapter
    }

    private fun initListener() {
        binding.swipeRefresh.setOnRefreshListener {
            listener?.reLoadMobileList()
        }
        adapter.setOnClickFavorite {
            listener?.onClickFavorite(it)
        }
        adapter.setOnClickViewDetail {
            listener?.onMobileDetail(it)
        }
    }


    override fun showMobileDetail(values: Pair<GraphMobile, MutableList<GraphImage>>) {
        if (userVisibleHint) {
            val mobile = values.first
            val images = values.second
            context?.let { MobileDetailActivity.starter(it, mobile, images) }
        }
    }

    override fun onSortList(sortEnum: SortEnum) {
        adapter.sortList(sortEnum)
    }

    override fun onLoadingState(isLoading: Boolean) {
        binding.swipeRefresh.isRefreshing = isLoading
    }


    override fun showMobileList(list: MutableList<GraphMobile>) {
        adapter.updateView(list)
    }

    override fun onDeleteFavorite(mobile: GraphMobile) {
        adapter.updateDeleteFavorite(mobile)
    }

    override fun injectDependencies(graph: ApplicationComponent) {
        graph.plus(MobileListModule(this))
                .injectTo(this)
    }
}
