package com.test.mobilephonebuyer.ui.favorite

import androidx.fragment.app.Fragment
import com.test.mobilephonebuyer.di.module.FragmentModule
import dagger.Module


@Module
class FavoriteListModule(fragment: Fragment) : FragmentModule(fragment) {

}