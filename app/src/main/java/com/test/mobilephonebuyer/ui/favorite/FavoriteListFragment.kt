package com.test.mobilephonebuyer.ui.favorite


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.adapter.MobileListAdapter
import com.test.mobilephonebuyer.adapter.SwipeToDeleteCallback
import com.test.mobilephonebuyer.custom.dialog.DialogLoading
import com.test.mobilephonebuyer.databinding.FragmentFavoriteListBinding
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.ui.base.fragment.BindingFragment
import com.test.mobilephonebuyer.ui.detail.MobileDetailActivity
import com.test.mobilephonebuyer.ui.main.OnSentEventMainActivityListener
import javax.inject.Inject


class FavoriteListFragment : BindingFragment<FragmentFavoriteListBinding>(), OnEventFavoriteListFragmentListener {

    @Inject
    lateinit var adapter: MobileListAdapter
    private var favorites = mutableListOf<GraphMobile>()
    private var listener: OnSentEventMainActivityListener? = null
    private lateinit var dialogLoading: DialogLoading


    companion object {
        fun newInstance(context: Context): Fragment {
            return instantiate(context, FavoriteListFragment::class.java.name)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnSentEventMainActivityListener
        if (listener == null) {
            throw ClassCastException("$context must implement OnSentEventMainActivityListener")
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_favorite_list
    }


    private fun initView() {
        adapter.setCurrentMode(MobileListAdapter.Mode.FAVORITE)
        binding.rvFavorite.adapter = adapter
    }

    private fun initListener() {
        adapter.setOnClickViewDetail {
            listener?.onMobileDetail(it)
        }
        enableSwipeToDelete()
    }

    override fun reLoadView() {
        adapter.clear()
    }

    override fun onLoadingState(isLoading: Boolean) {
        if (isLoading) {
            dialogLoading = DialogLoading.newInstance()
            dialogLoading.show(childFragmentManager)
        } else {
            dialogLoading.dismiss()
        }
    }

    override fun showMobileDetail(values: Pair<GraphMobile, MutableList<GraphImage>>) {
        if (userVisibleHint) {
            val mobile = values.first
            val images = values.second
            context?.let { MobileDetailActivity.starter(it, mobile, images) }
        }
    }

    override fun showFavoriteList(mobile: GraphMobile) {
        val mobileDuplicate = favorites.find { it.id == mobile.id }
        val isDuplicate = mobileDuplicate?.let { true } ?: false
        if (isDuplicate) {
            val position = favorites.indexOf(mobileDuplicate)
            adapter.removeItem(position)
        } else {
            favorites.add(mobile)
            adapter.updateView(favorites)
        }
    }


    override fun onSortList(sortEnum: SortEnum) {
        adapter.sortList(sortEnum)
    }


    private fun enableSwipeToDelete() {
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(context!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                val position = viewHolder.adapterPosition
                val item = adapter.getItem(position)
                item.isFavorite = false
                adapter.removeItem(position)
                listener?.onDeleteFavorite(item)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(binding.rvFavorite)
    }

    override fun injectDependencies(graph: ApplicationComponent) {
        graph.plus(FavoriteListModule(this))
            .injectTo(this)
    }

}
