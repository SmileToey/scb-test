package com.test.mobilephonebuyer.ui.list

import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.ui.main.OnEventBaseListener

interface OnEventMobileListFragmentListener : OnEventBaseListener {
    fun onDeleteFavorite(mobile: GraphMobile)
    fun showMobileList(list: MutableList<GraphMobile>)
}
