package com.test.mobilephonebuyer.ui.favorite

import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.ui.main.OnEventBaseListener

interface OnEventFavoriteListFragmentListener : OnEventBaseListener {
    fun reLoadView()
    fun showFavoriteList(mobile: GraphMobile)
}
