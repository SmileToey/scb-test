package com.test.mobilephonebuyer.ui.detail

import com.test.mobilephonebuyer.di.PerActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(
    modules = arrayOf(
        MobileDetailModule::class
    )
)
interface MobileDetailComponent {
    fun injectTo(activity: MobileDetailActivity)
}