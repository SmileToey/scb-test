package com.test.mobilephonebuyer.ui.list

import com.test.mobilephonebuyer.di.PerActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(
    modules = arrayOf(
        MobileListModule::class
    )
)
interface MobileListComponent {
    fun injectTo(fragment: MobileListFragment)
}