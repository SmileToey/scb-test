package com.test.mobilephonebuyer.ui.list

import androidx.fragment.app.Fragment
import com.test.mobilephonebuyer.di.module.FragmentModule
import dagger.Module


@Module
class MobileListModule(fragment: Fragment) : FragmentModule(fragment) {

}