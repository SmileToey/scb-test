package com.test.mobilephonebuyer.enums

enum class SortEnum(var id: Int) {
    PRICE_LOW_TO_HIGH(0),
    PRICE_HIGH_TO_LOW(1),
    RATING(2);

    companion object {
        fun fromId(id: Int): SortEnum {
            values()
                    .filter { id == it.id }
                    .forEach { return it }
            return PRICE_HIGH_TO_LOW
        }
    }

}