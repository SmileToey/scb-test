package com.test.mobilephonebuyer.enums

enum class ApplicationMode(val baseUrl: String) {
    Uat("https://scb-test-mobile.herokuapp.com/api/"),
    Developer("https://scb-test-mobile.herokuapp.com/api/"),
    Production("https://scb-test-mobile.herokuapp.com/api/");
}