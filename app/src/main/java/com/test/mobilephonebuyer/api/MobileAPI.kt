package com.test.mobilephonebuyer.api

import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface MobileAPI {

    @GET("mobiles")
    fun getMobileList(): Observable<MutableList<GraphMobile>>

    @GET("mobiles/{mobile_id}/images")
    fun getImagesWithMobileId(@Path("mobile_id") mobileId: Int): Observable<MutableList<GraphImage>>
}
