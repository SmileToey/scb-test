package com.test.mobilephonebuyer.extensions

import android.os.Bundle
import java.text.DecimalFormat


fun Double?.toCurrency(): String {
    return if (this != null) {
        val decimal = DecimalFormat("#,###.##")
        decimal.format(this)
    } else {
        "0"
    }
}

inline fun <reified T> Bundle.getList(keyValue: String): MutableList<T> {
    val array = this.getParcelableArray(keyValue)
    return if (array != null) {
        val list = arrayListOf<T>()
        for (a in array) {
            if (a is T) {
                list.add(a)
            }
        }
        list
    } else {
        arrayListOf()
    }
}