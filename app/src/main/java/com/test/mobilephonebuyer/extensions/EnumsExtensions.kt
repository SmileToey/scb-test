@file:Suppress("NOTHING_TO_INLINE")
package com.test.mobilephonebuyer.extensions


import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import java.util.*

// enum

inline fun Parcel.writeEnum(e: Enum<*>) {
    writeString(e.name)
}

inline fun <reified E : Enum<E>> Parcel.readEnum(): E =
        java.lang.Enum.valueOf(E::class.java, readString())

inline fun <E : Enum<E>> Parcel.readEnum(eClass: Class<E>): E =
        java.lang.Enum.valueOf(eClass, readString())


inline fun Bundle.putEnum(name: String, value: Enum<*>) {
    putString(name, value.name)
}

inline fun <reified E : Enum<E>> Bundle.getEnum(name: String): E =
        java.lang.Enum.valueOf(E::class.java, getString(name))

inline fun <E : Enum<E>> Bundle.getEnum(name: String, eClass: Class<E>): E =
        java.lang.Enum.valueOf(eClass, getString(name))


inline fun Intent.putEnumExtra(name: String, value: Enum<*>): Intent =
        putExtra(name, value.name)

inline fun <reified E : Enum<E>> Intent.getEnumExtra(name: String): E =
        java.lang.Enum.valueOf(E::class.java, getStringExtra(name))

inline fun <E : Enum<E>> Intent.getEnumExtra(name: String, eClass: Class<E>): E =
        java.lang.Enum.valueOf(eClass, getStringExtra(name))


// enumSet

fun <E : Enum<E>> Parcel.writeEnumSet(set: Set<E>) {
    writeStringArray(set.mapToArray { it.name })
}

inline fun <reified E : Enum<E>> Parcel.readEnumSet(): Set<E> =
        readEnumSet(E::class.java)

fun <E : Enum<E>> Parcel.readEnumSet(eClass: Class<E>): Set<E> =
        createStringArray().mapTo(EnumSet.noneOf(eClass)) { java.lang.Enum.valueOf(eClass, it) }


fun <E : Enum<E>> Bundle.putEnumSet(name: String, set: Set<E>) {
    putStringArray(name, set.mapToArray { it.name })
}

inline fun <reified E : Enum<E>> Bundle.getEnumSet(name: String): Set<E> =
        getEnumSet(name, E::class.java)

fun <E : Enum<E>> Bundle.getEnumSet(name: String, eClass: Class<E>): Set<E> =
        getStringArray(name).mapTo(EnumSet.noneOf(eClass)) { java.lang.Enum.valueOf(eClass, it) }


fun <E : Enum<E>> Intent.putEnumSetExtra(name: String, set: Set<E>) {
    putExtra(name, set.mapToArray { it.name })
}

inline fun <reified E : Enum<E>> Intent.getEnumSetExtra(name: String): Set<E> =
        getEnumSetExtra(name, E::class.java)

fun <E : Enum<E>> Intent.getEnumSetExtra(name: String, eClass: Class<E>): Set<E> =
        getStringArrayExtra(name).mapTo(EnumSet.noneOf(eClass)) { java.lang.Enum.valueOf(eClass, it) }

// internal

private inline fun <T, reified R> Collection<T>.mapToArray(transform: (T) -> R): Array<R> {
    val itr = iterator()
    val array = Array(size) { transform(itr.next()) }
    check(!itr.hasNext())
    return array
}