package com.test.mobilephonebuyer.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.test.mobilephonebuyer.R


var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

fun ImageView.loadImage(resId: Int) {
    val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .placeholder(R.drawable.ic_image_place_holder_24dp)
    Glide.with(context).load(resId).apply(requestOptions).into(this)
}


fun ImageView.loadImage(url: String) {
    try {
        val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.ic_image_place_holder_24dp)
        Glide.with(context).load(url).apply(requestOptions).into(this)
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }
}



