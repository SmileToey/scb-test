package com.test.mobilephonebuyer.presenter

import com.test.mobilephonebuyer.api.MobileAPI
import com.test.mobilephonebuyer.model.GraphImage
import com.test.mobilephonebuyer.model.GraphMobile
import com.test.mobilephonebuyer.rx.RxThread
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MobileListPresenter @Inject constructor(private val api: MobileAPI,
                                              private val rxThread: RxThread) {

    private lateinit var view: View
    private val disposable = CompositeDisposable()

    interface View : BaseViewInterface {
        fun showList(list: MutableList<GraphMobile>)
        fun showImages(images: MutableList<GraphImage>)
        fun showMessageError(message: String?)
    }

    fun injectView(view: View) {
        this.view = view
    }

    fun getMobileList() {
        view.loading()
        disposable.add(api.getMobileList()
                .compose(rxThread.applyAsync())
                .doOnTerminate {
                    view.dismissLoading()

                }
                .subscribe({
                    view.showList(it)
                }, {
                    view.showMessageError(it.message)
                })
        )
    }

    fun getImagesWithMobileId(mobileId: Int) {
        view.loading()
        disposable.add(api.getImagesWithMobileId(mobileId)
                .compose(rxThread.applyAsync())
                .doOnTerminate {
                    view.dismissLoading()

                }
                .subscribe({
                    view.showImages(it)
                }, {
                    view.showMessageError(it.message)
                })
        )
    }

    fun onDestroy() {
        disposable.clear()
    }
}
