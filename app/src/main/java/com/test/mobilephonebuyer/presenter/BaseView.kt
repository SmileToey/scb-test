package com.test.mobilephonebuyer.presenter

interface BaseViewInterface {
    fun loading()
    fun dismissLoading()
}