package com.test.mobilephonebuyer.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class GraphImage constructor(@SerializedName("id")
                             var id: Int = 0,
                             @SerializedName("mobile_id")
                             var mobileId: Int = 0,
                             @SerializedName("url")
                             var url: String = "") : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readInt(),
            source.readString()
    )


    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeInt(mobileId)
        writeString(url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GraphImage> = object : Parcelable.Creator<GraphImage> {
            override fun createFromParcel(source: Parcel): GraphImage = GraphImage(source)
            override fun newArray(size: Int): Array<GraphImage?> = arrayOfNulls(size)
        }
    }
}