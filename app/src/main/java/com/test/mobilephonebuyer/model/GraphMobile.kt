package com.test.mobilephonebuyer.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class GraphMobile constructor(@SerializedName("brand")
                              var brand: String = "",
                              @SerializedName("description")
                              var description: String = "",
                              @SerializedName("id")
                              var id: Int = 0,
                              @SerializedName("name")
                              var name: String = "",
                              @SerializedName("price")
                              var price: Double = 0.0,
                              @SerializedName("rating")
                              var rating: Double = 0.0,
                              @SerializedName("thumbImageURL")
                              var thumbImageURL: String = "",
                              var isFavorite: Boolean = false) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readDouble(),
            source.readDouble(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(brand)
        writeString(description)
        writeInt(id)
        writeString(name)
        writeDouble(price)
        writeDouble(rating)
        writeString(thumbImageURL)
        writeInt((if (isFavorite) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GraphMobile> = object : Parcelable.Creator<GraphMobile> {
            override fun createFromParcel(source: Parcel): GraphMobile = GraphMobile(source)
            override fun newArray(size: Int): Array<GraphMobile?> = arrayOfNulls(size)
        }
    }
}