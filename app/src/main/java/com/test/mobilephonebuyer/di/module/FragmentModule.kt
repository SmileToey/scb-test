package com.test.mobilephonebuyer.di.module

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides

@Module
abstract class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun provideFragment(): Fragment = fragment

    @Provides
    fun provideFragmentContext(): Context = fragment.context!!

    @Provides
    fun provideFragmentActivity(): Activity = fragment.activity!!

    @Provides
    fun provideFragmentManager(): FragmentManager = fragment.childFragmentManager
}