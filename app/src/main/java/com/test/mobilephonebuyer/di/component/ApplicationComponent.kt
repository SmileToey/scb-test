package com.test.mobilephonebuyer.di.component

import com.test.mobilephonebuyer.AppStart
import com.test.mobilephonebuyer.di.module.ApplicationModule
import com.test.mobilephonebuyer.di.module.HttpModule
import com.test.mobilephonebuyer.di.module.RxThreadModule
import com.test.mobilephonebuyer.ui.detail.MobileDetailComponent
import com.test.mobilephonebuyer.ui.detail.MobileDetailModule
import com.test.mobilephonebuyer.ui.favorite.FavoriteListComponent
import com.test.mobilephonebuyer.ui.favorite.FavoriteListModule
import com.test.mobilephonebuyer.ui.list.MobileListComponent
import com.test.mobilephonebuyer.ui.list.MobileListModule
import com.test.mobilephonebuyer.ui.main.MainComponent
import com.test.mobilephonebuyer.ui.main.MainModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
        modules = arrayOf(
                ApplicationModule::class,
                HttpModule::class,
                RxThreadModule::class
        )
)
interface ApplicationComponent {

    // Injectors
    fun injectTo(app: AppStart)

    // Submodule methods
    // Every screen is its own submodule of the graph and must be added here.

    /**
     * Activity
     */
    fun plus(module: MainModule): MainComponent

    fun plus(module: MobileDetailModule): MobileDetailComponent
    /**
     * Fragment
     */
    fun plus(module: MobileListModule): MobileListComponent

    fun plus(module: FavoriteListModule): FavoriteListComponent
}