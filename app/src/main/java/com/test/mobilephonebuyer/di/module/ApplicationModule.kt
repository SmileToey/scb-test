package com.test.mobilephonebuyer.di.module

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.test.mobilephonebuyer.AppStart
import com.test.mobilephonebuyer.di.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: AppStart) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(): Context = app.baseContext

    @Provides
    @Singleton
    fun provideResources(): Resources = app.resources


}