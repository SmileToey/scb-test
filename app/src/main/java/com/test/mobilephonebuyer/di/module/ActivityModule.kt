package com.test.mobilephonebuyer.di.module

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.test.mobilephonebuyer.di.ActivityContext
import com.test.mobilephonebuyer.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
abstract class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    fun provideActivityContext(): Context = activity.baseContext

    @Provides
    fun provideFragmentManager(): FragmentManager = activity.supportFragmentManager
}