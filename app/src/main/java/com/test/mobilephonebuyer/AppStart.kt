package com.test.mobilephonebuyer

import android.app.Application
import com.test.mobilephonebuyer.di.component.ApplicationComponent
import com.test.mobilephonebuyer.di.component.DaggerApplicationComponent
import com.test.mobilephonebuyer.di.module.ApplicationModule
import com.test.mobilephonebuyer.di.module.HttpModule


class AppStart : Application() {


    companion object {
        lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        initDependencyGraph()
    }

    private fun initDependencyGraph() {
        graph = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .httpModule(HttpModule())
            .build()
        graph.injectTo(this)
    }


}