package com.test.mobilephonebuyer.custom.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.test.mobilephonebuyer.custom.BaseDialogFragment
import com.test.mobilephonebuyer.databinding.DialogSortListBinding
import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.extensions.getEnum
import com.test.mobilephonebuyer.extensions.putEnum


class DialogSortList : BaseDialogFragment() {

    private lateinit var binding: DialogSortListBinding
    private val TAG = "dialog_sort_list"

    private var onSortList: ((SortEnum) -> Unit)? = null
    private var currentSort = SortEnum.RATING

    companion object {
        const val KEY_SORT = "key_sort"
        fun newInstance(sortEnum: SortEnum): DialogSortList {
            val fragment = DialogSortList()
            val bundle = Bundle()
            bundle.putEnum(KEY_SORT, sortEnum)
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<DialogSortListBinding>(inflater, com.test.mobilephonebuyer.R.layout.dialog_sort_list, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog.setCanceledOnTouchOutside(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            currentSort = it.getEnum(KEY_SORT)
        }
        initView()
        initListener()
    }

    private fun initView() {
        when (currentSort) {
            SortEnum.PRICE_LOW_TO_HIGH -> binding.rdoLowToHigh.isChecked = true
            SortEnum.PRICE_HIGH_TO_LOW -> binding.rdoHighToLow.isChecked = true
            SortEnum.RATING -> binding.rdoRating.isChecked = true
        }
    }

    private fun initListener() {
        binding.root.setOnClickListener {
            dismiss()
        }
        binding.rdoGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(radioGroup: RadioGroup?, i: Int) {
                val radioButton = binding.rdoGroup.findViewById<RadioButton>(i)
                val id = binding.rdoGroup.indexOfChild(radioButton)
                val sortEnum = SortEnum.fromId(id)
                onSortList?.invoke(sortEnum)
            }
        })
    }

    fun setOnSortList(onSortList: ((SortEnum) -> Unit)?) {
        this.onSortList = onSortList
    }

    fun show(manager: FragmentManager?) {
        show(manager, TAG)
    }


}