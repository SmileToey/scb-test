package com.test.mobilephonebuyer.custom

import android.content.DialogInterface
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.test.mobilephonebuyer.R

open class BaseDialogFragment : DialogFragment() {

    var isShowing = false

    override fun show(manager: FragmentManager?, tag: String?) {
        if (isShowing) return
        try {
            val ft = manager?.beginTransaction()
            ft?.add(this, tag)
            ft?.commitAllowingStateLoss()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
        isShowing = true
    }

    override fun onStart() {
        super.onStart()
        this.dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        this.dialog.window?.setBackgroundDrawableResource(R.color.color_background_black_dialog)
        this.dialog.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
    }

    override fun onDismiss(dialog: DialogInterface) {
        isShowing = false
        super.onDismiss(dialog)
    }

}