package com.test.mobilephonebuyer.custom.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.custom.BaseDialogFragment
import com.test.mobilephonebuyer.databinding.DialogLoadingBinding
import com.test.mobilephonebuyer.enums.SortEnum


class DialogLoading : BaseDialogFragment() {

    private lateinit var binding: DialogLoadingBinding
    private val TAG = "dialog_loading"

    private var onSortList: ((SortEnum) -> Unit)? = null
    private var currentSort = SortEnum.RATING

    companion object {
        fun newInstance(): DialogLoading {
            val fragment = DialogLoading()
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<DialogLoadingBinding>(inflater, R.layout.dialog_loading, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    fun show(manager: FragmentManager?) {
        show(manager, TAG)
    }


}