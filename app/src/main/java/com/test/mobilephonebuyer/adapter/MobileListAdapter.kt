package com.test.mobilephonebuyer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.databinding.ItemMobileListBinding
import com.test.mobilephonebuyer.enums.SortEnum
import com.test.mobilephonebuyer.extensions.loadImage
import com.test.mobilephonebuyer.extensions.toCurrency
import com.test.mobilephonebuyer.model.GraphMobile
import javax.inject.Inject

class MobileListAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = mutableListOf<GraphMobile>()
    private var currentMode = Mode.LIST
    private var currentSort: SortEnum? = null

    private var clickFavorite: ((GraphMobile) -> Unit)? = null
    private var clickViewDetail: ((GraphMobile) -> Unit)? = null

    enum class Mode {
        LIST, FAVORITE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemMobileListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_mobile_list,
            parent,
            false
        )
        return ItemHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemHolder -> bindItem(holder, position)
        }
    }


    private fun bindItem(holder: ItemHolder, position: Int) {
        val item = items[position]
        val binding = holder.binding

        if (currentMode == Mode.FAVORITE) {
            binding.ivFavorite.visibility = View.GONE
        }

        binding.ivImage.loadImage(item.thumbImageURL)
        binding.tvBrand.text = item.brand
        binding.tvDes.text = item.description
        binding.tvPrice.text =
            String.format(context.getString(R.string.item_mobile_string_format_price), item.price.toCurrency())
        binding.tvRating.text = String.format(context.getString(R.string.item_mobile_string_format_rating), item.rating)
        setViewFavorite(binding.ivFavorite, item.isFavorite)

        binding.ivFavorite.setOnClickListener {
            item.apply {
                isFavorite = !isFavorite
            }.run {
                setViewFavorite(binding.ivFavorite, isFavorite)
            }
            clickFavorite?.invoke(item)
        }
        binding.root.setOnClickListener {
            clickViewDetail?.invoke(item)
        }
    }

    private fun setViewFavorite(imageView: ImageView, isFavorite: Boolean) {
        imageView.setImageResource(
            if (isFavorite) {
                R.drawable.ic_favorite_24dp
            } else {
                R.drawable.ic_un_favorite_24dp
            }
        )
    }

    fun setCurrentMode(mode: Mode) {
        this.currentMode = mode
    }

    private fun sortList() {
        currentSort?.let { sort ->
            when (sort) {
                SortEnum.PRICE_LOW_TO_HIGH -> {
                    items.sortWith(compareBy { it.price })
                }
                SortEnum.PRICE_HIGH_TO_LOW -> {
                    items.sortWith(compareByDescending { it.price })
                }
                SortEnum.RATING -> {
                    items.sortWith(compareByDescending { it.rating })
                }
            }
        }
    }

    fun updateDeleteFavorite(item: GraphMobile) {
        val mobile = items.find { it.id == item.id }
        mobile?.apply {
            isFavorite = item.isFavorite
        }.run {
            val position = items.indexOf(mobile)
            notifyItemChanged(position)
        }
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getItem(position: Int): GraphMobile {
        return items[position]
    }

    fun sortList(sortEnum: SortEnum) {
        this.currentSort = sortEnum
        sortList()
        notifyDataSetChanged()
    }

    fun updateView(items: MutableList<GraphMobile>) {
        this.items = items
        sortList()
        notifyDataSetChanged()
    }

    fun setOnClickViewDetail(clickViewDetail: ((GraphMobile) -> Unit)?) {
        this.clickViewDetail = clickViewDetail
    }

    fun setOnClickFavorite(clickFavorite: ((GraphMobile) -> Unit)?) {
        this.clickFavorite = clickFavorite
    }

    class ItemHolder(val binding: ItemMobileListBinding) : RecyclerView.ViewHolder(binding.root)

}