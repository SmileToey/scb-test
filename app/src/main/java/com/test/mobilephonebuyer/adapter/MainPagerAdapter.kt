package com.test.mobilephonebuyer.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.test.mobilephonebuyer.ui.favorite.FavoriteListFragment
import com.test.mobilephonebuyer.ui.list.MobileListFragment
import javax.inject.Inject

class MainPagerAdapter @Inject constructor(val context: Context, fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

    val fragments = hashMapOf<Page, Fragment>()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            Page.LIST.position -> {
                MobileListFragment.newInstance(context)
            }
            Page.FAVORITE.position -> {
                FavoriteListFragment.newInstance(context)
            }
            else -> Fragment()
        }
    }

    override fun getCount(): Int {
        return Page.values().size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return Page.values()[position].title
    }

    enum class Page(var position: Int, var title: String) {
        LIST(0, "Mobile List"),
        FAVORITE(1, "Favourite");

        companion object {
            fun fromPosition(position: Int): Page {
                Page.values()
                    .filter { position == it.position }
                    .forEach { return it }
                return LIST
            }
        }
    }


}