package com.test.mobilephonebuyer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.mobilephonebuyer.R
import com.test.mobilephonebuyer.databinding.ItemMobilePreviewImageBinding
import com.test.mobilephonebuyer.extensions.loadImage
import com.test.mobilephonebuyer.model.GraphImage
import javax.inject.Inject

class PreviewImageAdapter @Inject constructor(context: Context) : RecyclerView.Adapter<PreviewImageAdapter.ImageHolder>() {

    private var images = mutableListOf<GraphImage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val binding = DataBindingUtil.inflate<ItemMobilePreviewImageBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_mobile_preview_image,
                parent,
                false
        )
        return ImageHolder(binding)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        val image = images[position]
        val binding = holder.binding
        binding.ivImage.loadImage(image.url)
    }

    fun setImages(images: MutableList<GraphImage>) {
        this.images = images
    }


    class ImageHolder(val binding: ItemMobilePreviewImageBinding) : RecyclerView.ViewHolder(binding.root)
}